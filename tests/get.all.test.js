const request = require('supertest')
const app = require('../server')
const mongoose = require('mongoose')
const fs = require('fs')

const {
    DB_PORT,
    DB_HOST,
    DB_USER,
    DB_PASS,
    DB_NAME
} = process.env

beforeAll(async () => {
    const url = `mongodb://${DB_USER}:${DB_PASS}@${DB_HOST}:${DB_PORT}/${DB_NAME}?authSource=admin`
    await mongoose.connect(url, { useNewUrlParser: true, useUnifiedTopology: true })
})

describe('Testing GET /planets/list request to Star Wars API ...', () => {

    it('Retrieving all planets from api without planet previously added', async () => {
        const res = await request(app)
            .get('/planets/list')
        expect(res.statusCode).toEqual(200)
        expect(res.body).toEqual([])
    });

    it('Retrieving all planets from api with planet previously added', async () => {

        let res = await request(app)
        .post('/planets/create')
        .send({
            name: 'Tatooine',
            climate: 'arid',
            terrain: 'desert',
            film_appearances: 5,
        })

        res = await request(app)
        .post('/planets/create')
        .send({
            name: 'Bespin',
            climate: 'temperate',
            terrain: 'gas giant'
        })

        res = await request(app)
            .get('/planets/list')

        expect(res.statusCode).toEqual(200)
        expect(res.body).toMatchObject([
            {
                name: 'Tatooine',
                climate: 'arid',
                terrain: 'desert',
                film_appearances: 5,
            },
            {
                name: 'Bespin',
                climate: 'temperate',
                terrain: 'gas giant',
                film_appearances: 1
            }

        ])
    });
})

afterAll(async () => {
    await mongoose.connection.db.dropCollection('planets');
    await mongoose.connection.close();
})
