const mongoose = require('mongoose');
const { Schema } = mongoose;

const PlanetSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    climate: { type: String },
    terrain: { type: String },
    film_appearances: { type: Number }
});

exports.Planets = mongoose.model('planets', PlanetSchema);