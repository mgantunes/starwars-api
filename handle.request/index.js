const { Planets } = require('../models/planets');
const { getFilmsByPlanetName } = require('../swapi');
const { checkName, checkPlanetAlreadyExist } = require('./validation');

/**
 * @name listPlanets
 * @async
 * @description Method used to retrieve all planets from the db
 * @returns {Array<Object>} planets list
 */
exports.listPlanets = async () => {
    let planets = await Planets.find({}).exec();
    return planets;
}

/**
 * @name getPlanetByName
 * @async
 * @description Method used to retrieve a planet from the db with name equals to param 'name'
 * @param {String} name
 * @returns {Array<Object>} planets list
 * @throws Will throw an error if the planet does not exist
 */
exports.getPlanetByName = async (name) => {
    let planet = await Planets.find({ name: new RegExp(`^${name}$`, 'i') }).exec();
    if(!planet.length) throw new Error('Object not found');
    else return planet;
}

/**
 * @name getPlanetById
 * @async
 * @description Method used to retrieve a planet from the db with id equals to param 'id'
 * @param {String} id
 * @returns {Object} planet found
 * @throws Will throw an error if the planet does not exist
 */
exports.getPlanetById = async (id) => {
    try {
        let planet = await Planets.findById(id).exec();
        return planet;
    } catch (error) {
        throw new Error('Object not found')
    }
}

/**
 * @name createPlanet
 * @async
 * @description Method used to create a planet in the db with the object sent by the user
 * @param {Object} data
 * @returns {Object} planet created
 * @throws Will throw an error if the name is not given by the user
 * @throws Will throw an error if the planet name already exists in the db
 * @throws Will throw an error if the planet object values does not match planet schema
 */
exports.createPlanet = async (data) => {
    try {
        checkName(data);
        await checkPlanetAlreadyExist(data);
        if(!data.film_appearances) 
            data.film_appearances = await getFilmsByPlanetName(data.name)
        const doc = new Planets(data);
        let planet = await doc.save();
        return planet;
    } catch (error) {
        throw new Error(error.message)
    }
}

/**
 * @name deletePlanet
 * @async
 * @description Method used to delete a planet in the db with the id sent by the user
 * @param {String} id
 * @returns {String} message
 * @throws Will throw an error if there is no object with the given id
 */
exports.deletePlanet = async (id) => {
    let res;
    try {
        res = await Planets.findByIdAndDelete(id).exec();
    } catch (error) {
        throw new Error('Error while deleting object')
    }
    return res;
}