const request = require('supertest')
const app = require('../server')
const mongoose = require('mongoose')
const fs = require('fs')

const {
    DB_PORT,
    DB_HOST,
    DB_USER,
    DB_PASS,
    DB_NAME
} = process.env

beforeAll(async () => {
    const url = `mongodb://${DB_USER}:${DB_PASS}@${DB_HOST}:${DB_PORT}/${DB_NAME}?authSource=admin`
    await mongoose.connect(url, { useNewUrlParser: true, useUnifiedTopology: true })
})

describe('Testing POST request to Star Wars API ...', () => {
    it('Sending object with film_appearances attribute', async () => {
        const res = await request(app)
            .post('/planets/create')
            .send({
                name: 'Tatooine',
                climate: 'arid',
                terrain: 'desert',
                film_appearances: 5,
            })
        expect(res.statusCode).toEqual(200)
        expect(res.body).toMatchObject({
            name: 'Tatooine',
            climate: 'arid',
            terrain: 'desert',
            film_appearances: 5,
        })
    });

    it('Sending object without film_appearances attribute', async () => {
        const res = await request(app)
            .post('/planets/create')
            .send({
                name: 'Bespin',
                climate: 'temperate',
                terrain: 'gas giant'
            })
        expect(res.statusCode).toEqual(200)
        expect(res.body).toMatchObject({
            name: 'Bespin',
            climate: 'temperate',
            terrain: 'gas giant',
            film_appearances: 1,
        })
    });

    it('Sending object with name attribute spelled wrong', async () => {
        const res = await request(app)
            .post('/planets/create')
            .send({
                nme: 'Endor',
                climate: 'temperate',
                terrain: 'forests, mountains, lakes'
            })
        expect(res.statusCode).toEqual(400)
        expect(res.body).toEqual({ message: 'Field "name" is requried' })
    });

    it('Sending object with film_appearances attribute with wrong type', async () => {
        const res = await request(app)
            .post('/planets/create')
            .send({
                name: 'Kamino',
                climate: 'temperate',
                terrain: 'ocean',
                film_appearances: 'tes',
            })
        expect(res.statusCode).toEqual(400)
        expect(res.body).toMatchObject({"message": 'planets validation failed: film_appearances: Cast to Number failed for value "tes" at path "film_appearances"'})
    });

    it('Sending two objects with same name', async () => {
        let res = await request(app)
            .post('/planets/create')
            .send({
                name: 'Coruscant',
                climate: 'temperate',
                terrain: 'cityscape, mountains'
            })

        res = await request(app)
            .post('/planets/create')
            .send({
                name: 'Coruscant',
                climate: 'temperate',
                terrain: 'cityscape, mountains'
            })
        expect(res.statusCode).toEqual(400)
        expect(res.body).toMatchObject({"message": `Planet with name Coruscant already exists`})
    });
})

afterAll(async () => {
    await mongoose.connection.db.dropCollection('planets');
    await mongoose.connection.close();
})