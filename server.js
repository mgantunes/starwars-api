const express = require('express');
const bodyParser = require('body-parser')

const {
    listPlanets, getPlanetByName, getPlanetById, createPlanet, deletePlanet
} = require('./handle.request');

let app = express();

app.use(bodyParser.json())

/**
 * @name GET /planets/list 
 * @function
 * @description Endpoint responsable for getting all planets from the db
 * @param req {Object} The request.
 * @param res {Object} The response.
 * @returns {Array<Object>} list of planets found
 */
app.get('/planets/list', async (req,res) => {
    try {
        let planets = await listPlanets();
        res.status(200).send(planets)
    } catch (error) {
        res.status(500).send('Internal Error')
    }
})

/**
 * @name GET /planets/name/:name 
 * @function
 * @description Endpoint responsable for getting a planet in the db with name given from the user 
 * @param req {Object} The request.
 * @param req.params.name {String} name of the planet
 * @param res {Object} The response.
 * @returns {Array<Object>} list of planets found
 */
app.get('/planets/name/:name', async (req,res) => {
    try {
        let planet = await getPlanetByName(req.params.name);
        res.status(200).send(planet)
    } catch (error) {
        res.status(404).send({ message: error.message })
    }
})

/**
 * @name GET /planets/id/:id
 * @function
 * @description Endpoint responsable for getting a planet in the db with id given from the user 
 * @param req {Object} The request.
 * @param req.params.id {String} id of the planet
 * @param res {Object} The response.
 * @returns {Object} planet found
 */
app.get('/planets/id/:id', async (req,res) => {
    try {
        let planet = await getPlanetById(req.params.id);
        res.status(200).send(planet)
    } catch (error) {
        res.status(404).send({ message: error.message })
    }
})

/**
 * @name POST /planets/create
 * @function
 * @description Endpoint responsable for creating a planet in the db with data given from the user 
 * @param req {Object} The request.
 * @param req.body.name {String} planet name
 * @param req.body.climate {String} planet climate
 * @param req.body.terrain {String} planet terrai
 * @param req.body.film_appearences {String} planet film_appearences - optional
 * @param res {Object} The response.
 * @returns {Object} planet created
 */
app.post('/planets/create', async (req,res) => {
    try {
        let planet = await createPlanet(req.body)
        res.status(200).send(planet)
    } catch (error) {
        res.status(400).send({ message: error.message })
    }
})

/**
 * @name DELETE /planets/:id
 * @function
 * @description Endpoint responsable for deleting a planet in the db with id given from the user 
 * @param req {Object} The request.
 * @param req.param.id {String} planet id
 * @param res {Object} The response.
 * @returns {String} message
 */
app.delete('/planets/:id', async (req,res) => {
    try {
        await deletePlanet(req.params.id);
        res.status(200).send({ message: `${req.params.id} delete successfully.`})
    } catch (error) {
        res.status(404).send({ message: error.message })
    }
})

module.exports = app;