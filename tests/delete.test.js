const request = require('supertest')
const app = require('../server')
const mongoose = require('mongoose')
const fs = require('fs')

const {
    DB_PORT,
    DB_HOST,
    DB_USER,
    DB_PASS,
    DB_NAME
} = process.env

beforeAll(async () => {
    const url = `mongodb://${DB_USER}:${DB_PASS}@${DB_HOST}:${DB_PORT}/${DB_NAME}?authSource=admin`
    await mongoose.connect(url, { useNewUrlParser: true, useUnifiedTopology: true })
})

describe('Testing DELETE /planets/:id request to Star Wars API ...', () => {

    it('Deleting existing planet from api', async () => {

        let res = await request(app)
        .post('/planets/create')
        .send({
            name: 'Tatooine',
            climate: 'arid',
            terrain: 'desert',
            film_appearances: 5,
        })

        let id = res.body._id;
        res = await request(app)
            .delete(`/planets/${id}`)
        expect(res.statusCode).toEqual(200)
        expect(res.body).toEqual({ message: `${id} delete successfully.`})
    });

    it('Deleting non existing planet from api', async () => {

        res = await request(app)
            .delete(`/planets/123`)
        expect(res.statusCode).toEqual(404)
        expect(res.body).toEqual({ message: 'Error while deleting object'})
    });

})

afterAll(async () => {
    await mongoose.connection.db.dropCollection('planets');
    await mongoose.connection.close();
})
