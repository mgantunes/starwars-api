const request = require('supertest')
const app = require('../server')
const mongoose = require('mongoose')
const fs = require('fs')

const {
    DB_PORT,
    DB_HOST,
    DB_USER,
    DB_PASS,
    DB_NAME
} = process.env

beforeAll(async () => {
    const url = `mongodb://${DB_USER}:${DB_PASS}@${DB_HOST}:${DB_PORT}/${DB_NAME}?authSource=admin`
    await mongoose.connect(url, { useNewUrlParser: true, useUnifiedTopology: true })
})

describe('Testing GET /planets/:id and /planets/:name request to Star Wars API ...', () => {

    it('Retrieving planet by name from api', async () => {
        let res = await request(app)
            .post('/planets/create')
            .send({
                name: 'Tatooine',
                climate: 'arid',
                terrain: 'desert',
                film_appearances: 5,
            })
        
        let name = res.body.name
        res = await request(app)
            .get(`/planets/name/${name}`)
        expect(res.statusCode).toEqual(200)
        expect(res.body).toMatchObject([{
            name: 'Tatooine',
            climate: 'arid',
            terrain: 'desert',
            film_appearances: 5,
        }])
    });

    it('Retrieving planet by id from api', async () => {
        let res = await request(app)
            .post('/planets/create')
            .send({
                name: 'Bespin',
                climate: 'temperate',
                terrain: 'gas giant'
            })
        
        let id = res.body._id
        res = await request(app)
            .get(`/planets/id/${id}`)
        expect(res.statusCode).toEqual(200)
        expect(res.body).toMatchObject({
            name: 'Bespin',
            climate: 'temperate',
            terrain: 'gas giant',
            film_appearances: 1,
        })
    });

    it('Trying to retrieve non existing planet by name from api', async () => {

        res = await request(app)
            .get(`/planets/name/Endor`)
        expect(res.statusCode).toEqual(404)
        expect(res.body).toEqual({ message: 'Object not found' })
    });

    it('Trying to retrieve non existing planet by id from api', async () => {

        res = await request(app)
            .get(`/planets/id/123`)
        expect(res.statusCode).toEqual(404)
        expect(res.body).toEqual({ message: 'Object not found' })
    });
})

afterAll(async () => {
    await mongoose.connection.db.dropCollection('planets');
    await mongoose.connection.close();
})
