const { Planets } = require('../models/planets');

/**
 * @name checkName
 * @description Method used validate if the field name exists inside object
 * @param {Object} data
 * @throws Will throw an error if there is no name attribute in param data
 */
exports.checkName = (data) => {
    if(data.name) return;
    throw new Error ('Field "name" is requried');
}

/**
 * @name checkPlanetAlreadyExist
 * @async
 * @description Method used validate if already exists a planet inside the db with the same name as data.name
 * @param {Object} data
 * @throws Will throw an error if there is already a planet with the given name
 */
exports.checkPlanetAlreadyExist = async (data) => {
    let planet = await Planets.find({ name: new RegExp(`^${data.name}$`, 'i') }).exec();
    if(planet.length) throw new Error(`Planet with name ${data.name} already exists`);
    else return;
}