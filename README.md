# starwars-api

# Build

## Create File .env 

Follow file `.env.example` to create a `.env` file in the root of the project<br>
Run `npm install`<br>
Run `docker-compose up -d`<br>
The api address will be `http://localhost:3000`

## Routes

- `GET /planets/list` - list all planets inside db
- `GET /planets/name/:name` - list all planets inside db with name equals `name`
- `GET /planets/id/:id` - get planet inside db with id equals `id`
- `POST /planets/create` - create new planet inside db
- `DELETE /planets/:id` - delete planet with id equals `id`

## Planet Schema

`{
    name: { type: String, required: true },
    climate: { type: String },
    terrain: { type: String },
    film_appearances: { type: Number }
}`

If the object sent to the `POST /planets/create` endpoint has `film_appearances` set, the object will be created with the value sent by the user. If the object sent DOES NOT have `film_appearances` set, the object will be created with `film_appearances` equals to the value retrieved from `https://swapi.dev/api/planets/?search=${name}` endpoint.
<br><br>

## Example

<br><br>
Object sent to the `POST /planets/create` endpoint

`{
    name: 'Tatooine',
    climate: 'arid',
    terrain: 'desert',
    film_appearances: 5,
}`

Value saved

`{
    name: 'Tatooine',
    climate: 'arid',
    terrain: 'desert',
    film_appearances: 5,
}`

Object sent to the `POST /planets/create` endpoint

`{
    name: 'Bespin',
    climate: 'temperate',
    terrain: 'gas giant'
}`

Value saved

`{
    name: 'Bespin',
    climate: 'temperate',
    terrain: 'gas giant'
    film_appearances: 1,
}`

# Test
Follow file `.test.env.example` to create a `.test.env` file in the root of the project <br>
`DB_USER` and `DB_PASS` need to the same as `.env`<br>
Run `npm install`<br>
Run `docker-compose up -d`<br>
Run `npm test`

