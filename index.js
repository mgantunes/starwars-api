const app = require('./server');
const db = require('./db');

db.start();

app.listen(3000);