const axios = require('axios');

/**
 * @name getFilmsByPlanetName
 * @async
 * @description Method used to get planet's film_appearences from the Star Wars public API.
 * This method uses the planet name given by the user to get the planet data.
 * @param {String} planetName
 * @returns {Number} Number of film appearences
 */
exports.getFilmsByPlanetName = async (planetName) => {
    let response = await axios.get(`https://swapi.dev/api/planets/?search=${planetName}`);
    let planet = response.data.results[0];
    return planet.films.length;
}