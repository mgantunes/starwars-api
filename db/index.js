const mongoose = require('mongoose');

const {
    DB_PORT,
    DB_HOST,
    DB_NAME,
    DB_USER,
    DB_PASS,
} = process.env

/**
 * @name start
 * @description Method used to connect to MongoDB
 */
exports.start = () => {
    mongoose.connect(`mongodb://${DB_USER}:${DB_PASS}@${DB_HOST}:${DB_PORT}/${DB_NAME}?authSource=admin`, { useNewUrlParser: true });

    mongoose.connection.on('open', () => console.log('Connected to Stars Wars API'))
    mongoose.connection.on('disconnected', () => console.log('Disconnected from Stars Wars API'))
}